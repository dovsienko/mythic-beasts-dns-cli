#!/usr/bin/env python3

# This file is a part of "CLI for Mythic Beasts APIs", a free software for
# command-line interface management of some services provided by Mythic Beasts.
# See the accompanying file "COPYING" for the copyright and licensing
# information.

from abc import ABC, abstractmethod
import argparse
from collections import OrderedDict
from configparser import ConfigParser
import os
import re
import requests
import sys
import urllib.parse

class MBDNSAPIRecord (ABC):
	SPECIAL = frozenset ([
		'_generated',
		# The "_template" flag ceased to exist in the DNS API on 2024-07-16.
	])
	MIN_TTL = 60
	MAX_TTL = 86400 * 7
	SPACE = re.compile (r'[ \t]+')
	QUOTED_STR = re.compile ('^"(.*)"$')
	HOSTNAME_CHARS = re.compile ('^[a-z0-9._@-]+$', re.IGNORECASE)
	SPLIT_HEX_STRING = re.compile ('^[0-9a-f]{2}(?: ?[0-9a-f]{2})*$', re.IGNORECASE)
	DIGITS = frozenset (['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])

	def __init__ (self, apirr: dict):
		if self.RRTYPE != apirr['type']:
			raise TypeError ("this class implements '%s', but the API data is for '%s'" %
				(self.RRTYPE, apirr['type']))
		self.apirr = dict (apirr)
		self.labels = None

	def format_data (self) -> str:
		'''Format RDATA as a master file string.

		This is roughly the opposite of parse_data(). RR types that override the
		latter or parse more than one token need to override this method.
		'''
		return self.apirr['data']

	@abstractmethod
	def assert_valid (self):
		'''Raise an exception if the RR object is invalid.

		Test that both the TTL and the hostname values look valid.  RR types can
		apply a more specific filter to the hostname if necessary. Test that each
		RDATA element is valid on its own, and that together all elements make a
		valid combination.  Every RR type needs to extend this method to test at
		least the "data" key.
		'''
		self.assert_in_interval (self.MIN_TTL, self.apirr['ttl'], self.MAX_TTL)
		self.assert_hostname_chars (self.apirr['host'])

	@staticmethod
	def prepare_data (value: str) -> dict:
		'''Convert argparse style RDATA elements to an API style dictionary.

		The input can come from either parse_data() or argparse.  RR types that
		need to do type or letter case conversion or IP address compression need
		to override this method.  The result needs to have "host", "ttl" and
		"type" defined to become a valid argument for the constructor.
		'''
		return {'data': value}

	@classmethod
	def parse_data (cls, data: str) -> list:
		'''Convert a master file RDATA string to argparse style elements.

		RR types that need to remove quotes or to join split hexstrings need to
		override this method, other RR types need to assign PARSE_TOKENS.
		'''
		if cls.PARSE_TOKENS == 1:
			return [data]
		else:
			return cls.SPACE.split (data, cls.PARSE_TOKENS - 1)

	def get_apirr (self) -> dict:
		'''Return a dictionary that can be immediately used with the API JSON.'''
		ret = dict (self.apirr)
		for k in self.SPECIAL:
			if k in ret:
				del (ret[k])
		return ret

	def zone_line (self) -> dict:
		'''Return a complete master file line.'''
		return {
			'name': self.apirr['host'],
			'type': self.apirr['type'],
			'class': 'IN',
			'ttl': self.apirr['ttl'],
			'rdata': self.format_data(),
			'comment': ', '.join (self.SPECIAL & set (self.apirr.keys())),
		}

	@classmethod
	def assert_hostname_chars (cls, s: str, pattern: re.Pattern = None):
		if pattern is None:
			pattern = cls.HOSTNAME_CHARS
		if pattern.match (s) is None:
			raise ValueError ("'%s' contains invalid characters for a hostname" % s)

	@staticmethod
	def assert_in_set (value, allowed: set):
		if value not in allowed:
			raise ValueError ("'%s' is not an allowed value" % value)

	@staticmethod
	def assert_in_interval (minval: int, value: int, maxval: int):
		'''Assert that the value belongs to the [minval, maxval] interval.'''
		if value < minval:
			raise ValueError ("%d is less than %d" % (value, minval))
		if value > maxval:
			raise ValueError ("%d is greater than %d" % (value, maxval))

	@classmethod
	def assert_hexstring (cls, hexstr: str, repetitions: str):
		if re.match ('^(?:[0-9a-f]{2})%s$' % repetitions, hexstr) is None:
			raise ValueError ("the hexstring '%s' does not have %s repetitions" % \
				(hexstr, repetitions))

	@classmethod
	def unquote_string (cls, s: str) -> str:
		m = cls.QUOTED_STR.match (s)
		if m is None:
			raise ValueError ("'%s' is not a double-quoted string" % s)
		return m.group (1)

	@classmethod
	def join_hexstring (cls, hexstr: str) -> str:
		if cls.SPLIT_HEX_STRING.match (hexstr) is None:
			raise ValueError ("'%s' is not a valid hex string" % hexstr)
		return hexstr.replace (' ', '')

	def __eq__ (self, other: object) -> bool:
		return self.apirr == other.get_apirr()

	def get_labels (self) -> list:
		'''Return the RR name as a two-level list for sorting comparison.

		Break the RR name down to a list of labels and break each label down to
		a list of strings and integers (in other word, split the labels on each
		string/integer boundary).  Cache the result because RR sorting will most
		likely reuse it.'''
		if self.labels is None:
			self.labels = []
			for labelstr in self.apirr['host'].lower().split ('.'):
				labeldata = []
				digital_buf = None
				buf = ''
				for char in labelstr:
					if digital_buf is None:
						digital_buf = char in self.DIGITS
					elif digital_buf and char not in self.DIGITS:
						labeldata.append (int (buf))
						buf = ''
						digital_buf = False
					elif not digital_buf and char in self.DIGITS:
						labeldata.append (buf)
						buf = ''
						digital_buf = True
					buf += char
				# Flush the buffer.
				if buf != '':
					labeldata.append (int (buf) if digital_buf else buf)
				self.labels.append (labeldata)
		return self.labels

	@staticmethod
	def lblcmp (a: list, b: list) -> int:
		'''Compare two parsed labels.

		Take two broken down labels and return a negative integer if the first label
		is "less" than the second, a positive integer if "greater" or else zero.'''
		for i in range (min (len (a), len (b))):
			if type (a[i]) == int and type (b[i]) == int:
				if a[i] != b[i]:
					return a[i] - b[i]
			else:
				astr = str (a[i])
				bstr = str (b[i])
				if astr < bstr:
					return -1
				if astr > bstr:
					return 1
		return len (a) - len (b)

	@classmethod
	def parsedcmp (cls, a: list, b: list) -> int:
		'''Compare two parsed resource record names.

		Take two broken down RR names and recursively compare them one label at a
		time, starting from the last (closest to the root) labels.  Return a
		negative integer if the first name is "less" than the second, a positive
		integer if "greater" or else zero.'''
		# The base case is either two roots or an RR of a subdomain and the
		# subdomain apex.
		if not len (a) or not len (b):
			return len (a) - len (b)
		# Try to break the tie before recursing.
		by_topmost = cls.lblcmp (a[len (a) - 1], b[len (b) - 1])
		if by_topmost:
			return by_topmost
		return cls.parsedcmp (a[:len (a) - 1], b[:len (b) - 1])

	def __lt__ (self, other: object) -> bool:
		by_labels = self.parsedcmp (self.get_labels(), other.get_labels())
		if by_labels < 0:
			return True
		if by_labels > 0:
			return False
		a = self.apirr['type']
		b = other.get_apirr()['type']
		if a != b:
			return a < b
		return self.format_data() < other.format_data()


class MBDNSAPIRecordA (MBDNSAPIRecord):
	RRTYPE = 'A'
	PARSE_TOKENS = 1
	IPV4_ADDR = re.compile (r'^[0-9]+(?:\.[0-9]+){3}$')

	@classmethod
	def prepare_ipv4_address (cls, s: str):
		from ipaddress import IPv4Address
		if cls.IPV4_ADDR.match (s) is None:
			raise ValueError ("not a valid IPv4 address: '%s'" % s)
		ret = IPv4Address (s)
		return ret.compressed

	def assert_valid (self):
		super().assert_valid()
		if self.apirr['data'] != self.prepare_ipv4_address (self.apirr['data']):
			raise ValueError ("not a compressed IPv4 address: '%s'" %
				self.apirr['data'])

	@classmethod
	def prepare_data (cls, value: str) -> dict:
		return {'data': cls.prepare_ipv4_address (value)}


class MBDNSAPIRecordAAAA (MBDNSAPIRecord):
	RRTYPE = 'AAAA'
	PARSE_TOKENS = 1
	IPV6_ADDR = re.compile ('^[0-9a-f:]+$', re.IGNORECASE)

	@classmethod
	def prepare_ipv6_address (cls, s: str) -> str:
		from ipaddress import IPv6Address
		if cls.IPV6_ADDR.match (s) is None:
			raise ValueError ("not a valid IPv6 address: '%s'" % s)
		ret = IPv6Address (s)
		return ret.compressed

	def assert_valid (self):
		super().assert_valid()
		if self.apirr['data'] != self.prepare_ipv6_address (self.apirr['data']):
			raise ValueError ("not a compressed IPv6 address: '%s'" %
				self.apirr['data'])

	@classmethod
	def prepare_data (cls, value: str) -> dict:
		return {'data': cls.prepare_ipv6_address (value)}


class MBDNSAPIRecordANAME (MBDNSAPIRecord):
	RRTYPE = 'ANAME'
	PARSE_TOKENS = 1

	def assert_valid (self):
		super().assert_valid()
		self.assert_hostname_chars (self.apirr['data'])


class MBDNSAPIRecordCAA (MBDNSAPIRecord):
	RRTYPE = 'CAA'
	VALID_FLAGS = frozenset ([0, 128])
	VALID_TAGS = frozenset ([
		'issue',
		'issuewild',
		'iodef',
	# Not supported by the API:
	#	'contactemail',
	#	'contactphone',
	])

	@classmethod
	def valid_flags (cls) -> list:
		return list (cls.VALID_FLAGS)

	@classmethod
	def valid_tags (cls) -> list:
		return list (cls.VALID_TAGS)

	def format_data (self) -> str:
		return "%u %s \"%s\"" % (
			self.apirr['caa_flags'],
			self.apirr['caa_tag'],
			self.apirr['data'])

	def assert_valid (self):
		super().assert_valid()
		self.assert_in_set (self.apirr['caa_flags'], self.VALID_FLAGS)
		self.assert_in_set (self.apirr['caa_tag'], self.VALID_TAGS)
		if self.apirr['caa_tag'] in ['issue', 'issuewild'] and self.apirr['data'] != ';':
			self.assert_hostname_chars (self.apirr['data'])

	@staticmethod
	def prepare_data (flags: str, tag: str, value: str) -> dict:
		return {
			'caa_flags': int (flags),
			'caa_tag': tag,
			'data': value,
		}

	@classmethod
	def parse_data (cls, data: str) -> list:
		flags, tag, value = cls.SPACE.split (data, 2)
		return [
			flags,
			tag,
			cls.unquote_string (value)
		]


class MBDNSAPIRecordCNAME (MBDNSAPIRecord):
	RRTYPE = 'CNAME'
	PARSE_TOKENS = 1

	def assert_valid (self):
		super().assert_valid()
		self.assert_hostname_chars (self.apirr['data'])


class MBDNSAPIRecordDNAME (MBDNSAPIRecord):
	RRTYPE = 'DNAME'
	PARSE_TOKENS = 1

	def assert_valid (self):
		super().assert_valid()
		self.assert_hostname_chars (self.apirr['data'])


class MBDNSAPIRecordMX (MBDNSAPIRecord):
	RRTYPE = 'MX'
	PARSE_TOKENS = 2

	def format_data (self) -> str:
		return"%u %s" % (self.apirr['mx_priority'], self.apirr['data'])

	def assert_valid (self):
		super().assert_valid()
		self.assert_in_interval (0, self.apirr['mx_priority'], 65535)
		self.assert_hostname_chars (self.apirr['data'])

	@staticmethod
	def prepare_data (priority: str, exchange: str) -> dict:
		return {
			'mx_priority': int (priority),
			'data': exchange,
		}


class MBDNSAPIRecordNS (MBDNSAPIRecord):
	RRTYPE = 'NS'
	PARSE_TOKENS = 1

	def assert_valid (self):
		super().assert_valid()
		self.assert_hostname_chars (self.apirr['data'])


class MBDNSAPIRecordPTR (MBDNSAPIRecord):
	RRTYPE = 'PTR'
	PARSE_TOKENS = 1

	def assert_valid (self):
		super().assert_valid()
		self.assert_hostname_chars (self.apirr['data'])


class MBDNSAPIRecordSRV (MBDNSAPIRecord):
	RRTYPE = 'SRV'
	PARSE_TOKENS = 4
	VALID_HOSTNAME = re.compile (r'^_[a-z0-9-]+\._[a-z]+\.[a-z0-9.@-]+$', re.IGNORECASE)

	def format_data (self) -> str:
		return "%u %u %u %s" % (
			self.apirr['srv_priority'],
			self.apirr['srv_weight'],
			self.apirr['srv_port'],
			self.apirr['data'])

	def assert_valid (self):
		super().assert_valid()
		self.assert_hostname_chars (self.apirr['host'], self.VALID_HOSTNAME)
		self.assert_in_interval (0, self.apirr['srv_priority'], 65535)
		self.assert_in_interval (0, self.apirr['srv_weight'], 65535)
		self.assert_in_interval (1, self.apirr['srv_port'], 65535)
		self.assert_hostname_chars (self.apirr['data'])

	@staticmethod
	def prepare_data (priority: str, weight: str, port: str, host: str):
		return {
			'srv_priority': int (priority),
			'srv_weight': int (weight),
			'srv_port': int (port),
			'data': host,
		}


class MBDNSAPIRecordSSHFP (MBDNSAPIRecord):
	RRTYPE = 'SSHFP'
	VALID_ALGOS = frozenset ([
		1, # RSA
		2, # DSA
		3, # ECDSA
		4, # Ed25519
	# Not supported by the API:
	#	6, # Ed448
	])
	VALID_TYPES = {
		1: '{20}', # SHA-1
		2: '{32}', # SHA-256
	}

	@classmethod
	def valid_algos (cls) -> list:
		return list (cls.VALID_ALGOS)

	@classmethod
	def valid_types (cls) -> list:
		return cls.VALID_TYPES.keys()

	def format_data (self) -> str:
		return "%u %u %s" % (
			self.apirr['sshfp_algorithm'],
			self.apirr['sshfp_type'],
			self.apirr['data'])

	def assert_valid (self):
		super().assert_valid()
		self.assert_in_set (self.apirr['sshfp_algorithm'], self.VALID_ALGOS)
		self.assert_in_set (self.apirr['sshfp_type'], self.VALID_TYPES.keys())
		self.assert_hexstring (self.apirr['data'], self.VALID_TYPES[self.apirr['sshfp_type']])

	@staticmethod
	def prepare_data (algorithm: str, hashtype: str, hexstr: str) -> dict:
		return {
			'sshfp_algorithm': int (algorithm),
			'sshfp_type': int (hashtype),
			'data': hexstr.lower(),
		}

	@classmethod
	def parse_data (cls, data: str) -> list:
		algorithm, htype, hexstr = cls.SPACE.split (data, 2)
		return [
			algorithm,
			htype,
			cls.join_hexstring (hexstr)
		]


class MBDNSAPIRecordTLSA (MBDNSAPIRecord):
	RRTYPE = 'TLSA'
	VALID_HOSTNAME = re.compile (r'^_[0-9]+\._[a-z]+\.[a-z0-9.@-]+$', re.IGNORECASE)
	VALID_USAGES = frozenset ([
		0, # PKIX-TA
		1, # PKIX-EE
		2, # DANE-TA
		3, # DANE-EE
	])
	VALID_SELECTORS = frozenset ([
		0, # full certificate
		1, # public key
	])
	VALID_MTYPES = {
		# 97 bytes to encode a full EC P-384 public key,
		# 513 bytes to encode a full RSA 4096 public key,
		# likely more for a full certificate.
		0: '{97,}', # all
		1: '{32}', # SHA-256
		2: '{64}', # SHA-512
	}

	@classmethod
	def valid_usages (cls) -> list:
		return list (cls.VALID_USAGES)

	@classmethod
	def valid_selectors (cls) -> list:
		return list (cls.VALID_SELECTORS)

	@classmethod
	def valid_mtypes (cls) -> list:
		return cls.VALID_MTYPES.keys()

	def format_data (self) -> str:
		return "%u %u %u %s" % (
			self.apirr['tlsa_usage'],
			self.apirr['tlsa_selector'],
			self.apirr['tlsa_matching'],
			self.apirr['data'])

	def assert_valid (self):
		super().assert_valid()
		self.assert_hostname_chars (self.apirr['host'], self.VALID_HOSTNAME)
		self.assert_in_set (self.apirr['tlsa_usage'], self.VALID_USAGES)
		self.assert_in_set (self.apirr['tlsa_selector'], self.VALID_SELECTORS)
		self.assert_in_set (self.apirr['tlsa_matching'], self.VALID_MTYPES.keys())
		self.assert_hexstring (self.apirr['data'], self.VALID_MTYPES[self.apirr['tlsa_matching']])

	@staticmethod
	def prepare_data (usage: str, selector: str, mtype: str, hexstr: str) ->dict:
		return {
			'tlsa_usage': int (usage),
			'tlsa_selector': int (selector),
			'tlsa_matching': int (mtype),
			'data': hexstr.lower(),
		}

	@classmethod
	def parse_data (cls, data: str) -> list:
		usage, selector, mtype, hexstr = cls.SPACE.split (data, 3)
		return [
			usage,
			selector,
			mtype,
			cls.join_hexstring (hexstr),
		]


class MBDNSAPIRecordTXT (MBDNSAPIRecord):
	RRTYPE = 'TXT'

	def format_data (self) -> str:
		return "\"%s\"" % self.apirr['data']

	def assert_valid (self):
		# It would be possible to validate SPF syntax in assert_valid() when the
		# value begins with "v=spf1 ".  For now accept any value as valid.
		super().assert_valid()

	@classmethod
	def parse_data (cls, data: str) -> list:
		return [cls.unquote_string (data)]


class MBDNSAPIRecordFactory:
	CLASSMAP = {
		'A': MBDNSAPIRecordA,
		'AAAA': MBDNSAPIRecordAAAA,
		'ANAME': MBDNSAPIRecordANAME,
		'CAA': MBDNSAPIRecordCAA,
		'CNAME': MBDNSAPIRecordCNAME,
		'DNAME': MBDNSAPIRecordDNAME,
		'MX': MBDNSAPIRecordMX,
		'NS': MBDNSAPIRecordNS,
		'PTR': MBDNSAPIRecordPTR,
		'SRV': MBDNSAPIRecordSRV,
		'SSHFP': MBDNSAPIRecordSSHFP,
		'TLSA': MBDNSAPIRecordTLSA,
		'TXT': MBDNSAPIRecordTXT,
	}

	@classmethod
	def rrtypes (cls) -> list:
		return cls.CLASSMAP.keys()

	@classmethod
	def rrclass (cls, rrtype: str):
		if rrtype.upper() not in cls.CLASSMAP:
			raise ValueError ("record type '%s' is not implemented" % rrtype)
		return cls.CLASSMAP[rrtype.upper()]

	@classmethod
	def from_apirr (cls,
		apirr: dict) -> MBDNSAPIRecord:
		rrclass = cls.rrclass (apirr['type'])
		return rrclass (apirr)

	@classmethod
	def from_argparse_arg (cls,
		ttl: str, rrtype: str, arg: list) -> MBDNSAPIRecord:
		rrclass = cls.rrclass (rrtype.upper())
		apirr = rrclass.prepare_data (*arg[1:])
		apirr.update ({
			'ttl': int (ttl),
			'host': arg[0],
			'type': rrtype.upper(),
		})
		ret = rrclass (apirr)
		ret.assert_valid()
		return ret

	@classmethod
	def from_master_file_line (cls,
		ttl: str, rrtype: str, host: str, rdata: str) -> MBDNSAPIRecord:
		rrclass = cls.rrclass (rrtype.upper())
		apirr = rrclass.prepare_data (*rrclass.parse_data (rdata))
		apirr.update ({
			'ttl': int (ttl),
			'host': host,
			'type': rrtype.upper(),
		})
		ret = rrclass (apirr)
		ret.assert_valid()
		return ret


class MBAPIError (RuntimeError):
	HTTP_TO_DESCR = {
		200: 'OK',
		400: 'bad input',
		401: 'not authenticated',
		403: 'not authorized',
		409: 'conflict',
		503: 'service unavailable',
	}
	HTTP_TO_EXIT = {
		400: os.EX_DATAERR,
		401: os.EX_NOUSER,
		403: os.EX_NOPERM,
		# This client does not implement name server verification.
		409: os.EX_CANTCREAT,
		503: os.EX_CANTCREAT,
	}
	AUTH_VALUE = re.compile ('^([^ ]+) .+')

	def __init__ (self, resp: requests.Response, is_documented: bool):
		self.resp = resp
		self.is_documented = is_documented
		self.details = []
		if resp.status_code == 400:
			try:
				self.details = resp.json()['errors']
			except requests.exceptions.JSONDecodeError:
				pass

	def get_diagnostics (self):
		headers = {}
		for k, v in self.resp.request.headers.items():
			if k == 'Authorization':
				v = self.AUTH_VALUE.sub (r'\1 ********', v)
			headers[k] = v
		return [
			('request method', self.resp.request.method),
			('request URL', self.resp.request.url),
			('request headers', headers),
			('request body', self.resp.request.body),
			('response body', self.resp.text),
		]

	def get_exit_status (self):
		return self.HTTP_TO_EXIT.get (self.resp.status_code, os.EX_PROTOCOL)

	def __str__ (self):
		return "server returned HTTP status code %u (%s)" % (
			self.resp.status_code,
			self.HTTP_TO_DESCR.get (self.resp.status_code, 'unknown')
		)


class MBAPIClient (ABC):
	@abstractmethod
	def __init__ (self):
		pass

	def assert_success (self, response: requests.Response, documented: list = [],
		expected: int = 200):
		'''Raise an exception if the request failed.'''
		if response.status_code != expected:
			raise MBAPIError (response, response.status_code in documented)


# https://www.mythic-beasts.com/support/api/auth
class MBAuthServiceClient (MBAPIClient):
	'''Provide an interface to the Auth service.'''
	AUTH_URL = 'https://auth.mythic-beasts.com/login'

	def __init__ (self, key_id: str, key_secret: str):
		self.key_id = key_id
		self.key_secret = key_secret
		self.auth_token = None
		self.user_agent = 'mbcli (%s)' % requests.utils.default_headers()['User-Agent']
		self.timeout = None

	def set_timeout (self, timeout: float):
		if timeout is not None and timeout <= 0:
			raise ValueError ('Invalid timeout %f' % timeout)
		self.timeout = timeout

	def get_auth_token (self):
		'''Obtain an authentication token.'''
		if self.auth_token is None:
			resp = requests.post (
				self.AUTH_URL,
				auth = (self.key_id, self.key_secret),
				headers = {'User-Agent': self.user_agent},
				timeout = self.timeout,
				data = {'grant_type': 'client_credentials'}
			)
			self.assert_success (resp)
			self.auth_token = resp.json()['access_token']
		return self.auth_token

	def request (self, method: str, url: str, **kwargs) -> object:
		'''Make an authenticated request.'''
		headers = {
			'Authorization': "Bearer %s" % self.get_auth_token(),
			'Accept': 'application/json',
			'User-Agent': self.user_agent,
		}
		return requests.request (method, url, headers = headers,
			timeout = self.timeout, **kwargs)


class MBAuthServiceDependant (MBAPIClient):
	@abstractmethod
	def __init__ (self, auth: MBAuthServiceClient, base_url: str):
		self.auth = auth
		self.base_url = base_url

	def authreq (self, method: str, url: str, **kwargs) -> object:
		'''Make an authenticated request.'''
		return self.auth.request (method, self.base_url + url, **kwargs)


# https://www.mythic-beasts.com/support/api/dnsv2
class MBDNSAPIClient (MBAuthServiceDependant):
	def __init__ (self, auth: MBAuthServiceClient, ipv: int = None):
		if ipv is None:
			base_url = 'https://api.mythic-beasts.com/dns/v2/'
		elif ipv == 4:
			base_url = 'https://ipv4.api.mythic-beasts.com/dns/v2/'
		elif ipv == 6:
			base_url = 'https://ipv6.api.mythic-beasts.com/dns/v2/'
		else:
			raise ValueError ("'%d' is not a valid value" % ipv)
		super().__init__ (auth, base_url)

	def get_record_types (self) -> dict:
		'''Return all RR types the API supports.'''
		resp = self.authreq ('GET', 'record-types')
		self.assert_success (resp)
		return resp.json()

	def get_zones (self) -> list:
		'''Return all zones available to the current API key.'''
		resp = self.authreq ('GET', 'zones')
		self.assert_success (resp, [401, 403])
		return resp.json()['zones']

	def get_zone_records (self, zone: str, rrfilter: dict = {}) -> list:
		'''Return all RRs in a zone.'''
		path = "zones/%s/records" % zone
		if len (rrfilter):
			path += '?' + urllib.parse.urlencode (rrfilter)
		resp = self.authreq ('GET', path)
		self.assert_success (resp, [401, 403])
		return sorted ([MBDNSAPIRecordFactory.from_apirr (apirr)
			for apirr in resp.json()['records']])

	def delete_zone_records (self, zone: str, rrname: str, rrtype: str) -> str:
		'''Delete all RRs that match the given name and type.'''
		resp = self.authreq ('DELETE', "zones/%s/records/%s/%s" % (zone, rrname, rrtype))
		self.assert_success (resp, [401, 403])
		return resp.json()['message']

	def add_replace_zone_record (self, http_method: str, zone: str,
		rr: MBDNSAPIRecord) -> str:
		'''Add a new RR.'''
		# A currently undocumented peculiarity of the API is that the following
		# URL paths only work when the API key has wildcard permits:
		# /zones/{zone}/records/{host} (any record type)
		# /zones/{zone}/records (any record name)
		rr = rr.get_apirr()
		resp = self.authreq (http_method,
			"zones/%s/records/%s/%s" % (zone, rr['host'], rr['type']),
			json = {'records': [rr]})
		self.assert_success (resp, [400, 401, 403])
		jsondict = resp.json()
		# For the PUT method the message string does not tell how many RRs
		# the request has removed.
		return jsondict['message'] \
			if http_method != 'PUT' \
			else '%u records added, %u records removed' % (
				jsondict['records_added'], jsondict['records_removed']
			)

	def add_zone_record (self, *args, **kwargs) -> str:
		return self.add_replace_zone_record ('POST', *args, **kwargs)

	def replace_zone_record (self, *args, **kwargs) -> str:
		return self.add_replace_zone_record ('PUT', *args, **kwargs)

	def update_dynamic (self, zone: str, rrname: str) -> str:
		'''Update a dynamic A/AAAA resource record.'''
		resp = self.authreq ('PUT', "zones/%s/dynamic/%s" % (zone, rrname))
		self.assert_success (resp, [400, 401, 403])
		return resp.json()['message']


# https://www.mythic-beasts.com/support/api/domains
class MBDomainAPIClient (MBAuthServiceDependant):
	'''Provide an interface to the Domains API.'''
	BASE_URL = 'https://api.mythic-beasts.com/beta/domains'

	def __init__ (self, auth: MBAuthServiceClient):
		super().__init__ (auth, self.BASE_URL)

	def get_domain_list (self) -> list:
		'''Return the list of domains available for the API key.'''
		resp = self.authreq ('GET', '')
		self.assert_success (resp, [403])
		return resp.json()['domains']

	def get_domain (self, name: str) -> dict:
		'''Retrieve all domain data and return as a single dictionary.'''
		resp = self.authreq ('GET', '/%s' % name)
		self.assert_success (resp, [403])
		ret = resp.json()
		if ret['status'] == 'registered':
			resp = self.authreq ('GET', '/%s/nameservers' % name)
			self.assert_success (resp, [400, 403])
			ret['nameservers'] = resp.json()['nameservers']
			if ret['dnssec']:
				resp = self.authreq ('GET', '/%s/ds' % name)
				self.assert_success (resp, [400, 403])
				ret['ds'] = resp.json()['records']
		return ret

	def set_lock (self, name: str, on: bool):
		'''Manage domain lock.'''
		resp = self.authreq ('PUT', "/%s/lock" % name,
			json = {'lock': on}
		)
		self.assert_success (resp, [400, 403])

	def set_dns (self, name: str, on: bool):
		'''Manage domain DNS service status.'''
		resp = self.authreq ('PUT', "/%s/dns" % name,
			json = {'dns': on}
		)
		self.assert_success (resp, [400, 403])

	def set_dnssec (self, name: str, on: bool):
		'''Manage domain DNSSEC service status.'''
		resp = self.authreq ('PUT', "/%s/dnssec" % name,
			json = {'dnssec': on}
		)
		self.assert_success (resp, [400, 403])


# https://www.mythic-beasts.com/support/api/raspberry-pi
class MBRPiAPIClient (MBAuthServiceDependant):
	'''Provide an interface to the Paspberry Pi Provisioning API.'''
	BASE_URL = 'https://api.mythic-beasts.com/beta/pi'

	def __init__ (self, auth: MBAuthServiceClient):
		super().__init__ (auth, self.BASE_URL)

	def get_images (self, model: int) -> dict:
		'''Retrieve all OS images for the RPi model.'''
		resp = self.authreq ('GET', "/images/%u" % model)
		self.assert_success (resp, [400])
		return resp.json()

	def get_servers (self) -> dict:
		'''Retrieve the list of all RPi servers in the account.'''
		resp = self.authreq ('GET', '/servers')
		self.assert_success (resp, [403])
		return resp.json()['servers']

	def get_server (self, serverid: str) -> dict:
		'''Retrieve detailed information about an RPi server.'''
		resp = self.authreq ('GET', "/servers/%s" % serverid)
		self.assert_success (resp, [403])
		return resp.json()

	def get_authorized_keys (self, serverid: str) -> str:
		'''Retrieve authorized public SSH keys for an RPi server.'''
		resp = self.authreq ('GET', "/servers/%s/ssh-key" % serverid)
		self.assert_success (resp, [403])
		# The API returns the file with DOS newlines.
		return resp.json()['ssh_key'].replace ("\r\n", "\n")

	def set_authorized_keys (self, serverid: str, keys: str):
		'''Replace authorized public SSH keys for an RPi server.'''
		resp = self.authreq ('PUT', "/servers/%s/ssh-key" % serverid,
			json = {'ssh_key': keys}
		)
		self.assert_success (resp, [403])

	def reboot (self, serverid: str):
		'''Reboot a Raspberry Pi server.'''
		resp = self.authreq ('POST', "/servers/%s/reboot" % serverid)
		self.assert_success (resp, [403, 409])
		return resp.json()['message']

	def set_power (self, serverid: str, on: bool):
		'''Manage a Raspberry Pi server power supply.'''
		resp = self.authreq ('PUT', "/servers/%s/power" % serverid,
			json = {'power': on}
		)
		self.assert_success (resp, [400, 403])
		return resp.json()['message']

	def provision (self, serverid: str, disk: int = None, ssh_key: str = None,
		model: int = None, os_image: str = None, wait_for_dns: bool = None):
		'''Provision a new Raspberry Pi server.'''
		kwargs = {}
		if disk is not None:
			kwargs['disk'] = disk
		if ssh_key is not None:
			kwargs['ssh_key'] = ssh_key
		if model is not None:
			kwargs['model'] = model
		if os_image is not None:
			kwargs['os_image'] = os_image
		if wait_for_dns is not None:
			kwargs['wait_for_dns'] = wait_for_dns
		resp = self.authreq ('POST', "/servers/%s" % serverid,
			json = kwargs if len (kwargs) else None
		)
		self.assert_success (resp, [400, 403, 409, 503], 202)

	def unprovision (self, serverid: str):
		'''Unprovision a Raspberry Pi server.'''
		resp = self.authreq ('DELETE', "/servers/%s" % serverid)
		self.assert_success (resp, [403])
		return resp.json()['message']


# https://www.mythic-beasts.com/support/api/vps
class MBVSAPIClient (MBAuthServiceDependant):
	'''Provide a read-only interface to the Virtual Server Provisioning API.'''
	BASE_URL = 'https://api.mythic-beasts.com/beta/vps'

	def __init__ (self, auth: MBAuthServiceClient):
		super().__init__ (auth, self.BASE_URL)

	def get_products (self) -> dict:
		'''Retrieve the list of available virtual server products.'''
		resp = self.authreq ('GET', '/products')
		self.assert_success (resp)
		return resp.json()

	def get_images (self) -> dict:
		'''Retrieve all OS images.'''
		resp = self.authreq ('GET', "/images")
		self.assert_success (resp)
		return resp.json()

	def get_servers (self) -> dict:
		'''Retrieve the list of all virtual servers in the account.'''
		resp = self.authreq ('GET', '/servers')
		self.assert_success (resp, [403])
		return resp.json()

	def get_server (self, vs_id: str) -> dict:
		'''Retrieve detailed information about the specified virtual server.'''
		resp = self.authreq ('GET', '/servers/%s' % vs_id)
		self.assert_success (resp, [403])
		return resp.json()

	def get_console_output (self, vs_id: int, limit: int = None) -> str:
		resp = self.authreq ('GET', '/servers/%s/console-output' % vs_id,
			params = None if limit is None else {'limit': limit})
		self.assert_success (resp, [400, 403])
		return resp.json()['console_output']


class MBCLIParamsError (RuntimeError):
	pass


class MBCLIParams:
	def __init__ (self, parsed: dict, configured: ConfigParser):
		self.parsed = parsed
		self.configured = configured

	def cmdline_none (self, key: str):
		return self.parsed.get (key)

	def config_none (self, section: str, key: str):
		if section in self.configured.sections() and key in self.configured[section]:
			return self.configured[section][key]
		return None

	def config (self, section: str, key: str):
		if section not in self.configured.sections():
			raise MBCLIParamsError ("the [%s] section is missing" % section)
		if key not in self.configured[section]:
			raise MBCLIParamsError ("the [%s] section does not define '%s'" % (section, key))
		return self.configured[section][key]

	@staticmethod
	def undo_argparse (s: str) -> str:
		return s.replace ('_', '-')

	def cmdline_config (self, section: str, key: str):
		if self.parsed[key] is not None:
			return self.parsed[key]
		if section not in self.configured.sections():
			raise MBCLIParamsError ("no '--%s' argument and the [%s] section is missing" %
				(self.undo_argparse (key), section))
		if key not in self.configured[section]:
			raise MBCLIParamsError ("no '--%s' argument and the [%s] section does not define '%s'" %
				(self.undo_argparse (key), section, key))
		return self.configured[section][key]

	def cmdline_config_none (self, section: str, key: str):
		if self.parsed[key] is not None:
			return self.parsed[key]
		if section in self.configured.sections() and key in self.configured[section]:
			return self.configured[section][key]
		return None


class MBCLICommand (ABC):
	STRTOBOOL = {
		'on': True,
		'off': False,
		'1': True,
		'0': False,
		'true': True,
		'false': False,
		'enable': True,
		'disable': False,
	}

	def __init__ (self, params: MBCLIParams):
		self.params = params

	@classmethod
	def make_parser (cls, subparser: object) -> argparse.ArgumentParser:
		parser = subparser.add_parser (cls.NAME, help = cls.HELP)
		parser.set_defaults (worker = cls)
		for argname, kwargs in cls.make_args().items():
			parser.add_argument (argname, **kwargs)
		return parser

	@staticmethod
	def make_args() -> OrderedDict:
		return OrderedDict()

	@abstractmethod
	def run (self):
		self.authclient = MBAuthServiceClient (
			self.params.config ('API', 'key_id'),
			self.params.config ('API', 'key_secret')
		)
		# Specific API client classes will use this instance to make their
		# requests, which will be subject to the same timeout.
		timeout = self.params.cmdline_config_none ('API', 'timeout')
		if timeout is not None:
			# Comes as a string if from configparser.
			self.authclient.set_timeout (float (timeout))

	@staticmethod
	def print_list_default (l: list, d):
		for each in l:
			print ("%s%s" % (each, ' (default)' if each == d else ''))

	@staticmethod
	def sorted_tuples (d: dict) -> list:
		'''Sort a dictionary by the keys and convert to items.'''
		return [(k, d[k]) for k in sorted (d.keys())]

	@staticmethod
	def print_header_data (rows: list):
		'''Print a list of pairs as a table with auto-sized left column.'''
		maxwidth = max ([len (header) for header, data in rows])
		for header, data in rows:
			print ('%-*s %s' % (maxwidth + 1, header + ':', data))


class MBCLICommandDNS (MBCLICommand):
	NAME = 'dns'
	HELP = 'Manage hosted DNS zones.'

	@staticmethod
	def parse_rr_type (s: str) -> str:
		if s.upper() not in MBDNSAPIRecordFactory.rrtypes():
			raise argparse.ArgumentTypeError ("'%s' is not a valid RR type" % s)
		return s.upper()

	@abstractmethod
	def run (self):
		super().run()
		self.dnsclient = MBDNSAPIClient (
			self.authclient,
			self.params.cmdline_none ('ipver')
		)


class MBCLICommandDNSRRTypes (MBCLICommandDNS):
	NAME = 'rrtypes'
	HELP = 'List all supported resource record types.'

	def run (self):
		super().run()
		rrtypes = self.sorted_tuples (self.dnsclient.get_record_types())
		self.print_header_data ([(k, ', '.join (v)) for k, v in rrtypes])


class MBCLICommandDNSZones (MBCLICommandDNS):
	NAME = 'zones'
	HELP = 'List all available DNS zones.'

	def run (self):
		super().run()
		self.print_list_default (
			self.dnsclient.get_zones(),
			self.params.config_none ('default', 'zone')
		)


class MBCLICommandDNSWithZone (MBCLICommandDNS):
	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--zone'] = {
			'help': 'use this DNS zone'
		}
		return ret

	@abstractmethod
	def run (self):
		super().run()
		self.zone_name = self.params.cmdline_config ('default', 'zone')

	def flag_zone_suffix (self, rrname: str):
		if rrname.endswith ('.' + self.zone_name):
			# looks like a zone record
			alt = rrname[:-1 - len (self.zone_name)]
		elif rrname == self.zone_name:
			# looks like the zone apex
			alt = '@'
		else:
			return
		print ('WARNING: "%s" means "%s", did you mean "%s" instead?' % (
				rrname,
				rrname + '.' + self.zone_name + '.',
				alt,
			)
		)


class MBCLICommandDNSLs (MBCLICommandDNSWithZone):
	NAME = 'ls'
	HELP = 'List all resource records in a zone.'

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--name'] = {
			'metavar': 'RRNAME',
			'help': 'limit results to the specified RR name (w/o the zone)'
		}
		ret['--type'] = {
			'metavar': 'RRTYPE',
			'type': cls.parse_rr_type,
			'help': 'limit results to the specified RR type'
		}
		return ret

	def run (self):
		super().run()
		rrfilter = {}
		if self.params.parsed['name'] is not None:
			rrfilter['host'] = self.params.parsed['name']
			self.flag_zone_suffix (self.params.parsed['name'])
		if self.params.parsed['type'] is not None:
			rrfilter['type'] = self.params.parsed['type']
		records = self.dnsclient.get_zone_records (self.zone_name, rrfilter)
		if not len (records):
			print ('; (empty result set)')
			return
		print ("; Resource records in %s:" % self.zone_name)
		zone_lines = [rr.zone_line() for rr in records]
		name_width = max ([len (zl['name']) for zl in zone_lines])
		ttl_width = max (len ('%u' % zl['ttl']) for zl in zone_lines)
		type_width = max ([len (s) for s in MBDNSAPIRecordFactory.rrtypes()])
		for zl in zone_lines:
			print ("%-*s  %*u  %-*s  %s%s" % (
				name_width,
				zl['name'],
				ttl_width,
				zl['ttl'],
				type_width,
				zl['type'],
				zl['rdata'],
				'  ; %s' % zl['comment'] if len (zl['comment']) else ''
			))


class MBCLICommandDNSRm (MBCLICommandDNSWithZone):
	NAME = 'rm'
	HELP = 'Remove all resource records that have the specified name and type.'

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['name'] = {
			'metavar': 'NAME',
			'help': 'resource record name (w/o the zone)',
		}
		ret['type'] = {
			'metavar': 'TYPE',
			'type': cls.parse_rr_type,
			'help': 'resource record type',
		}
		return ret

	def run (self):
		super().run()
		self.flag_zone_suffix (self.params.parsed['name'])
		msg = self.dnsclient.delete_zone_records (
			self.zone_name,
			self.params.parsed['name'],
			self.params.parsed['type']
		)
		print ('OK: %s' % msg)


class MBCLICommandDNSUpdDynamic (MBCLICommandDNSWithZone):
	NAME = 'upd-dynamic'
	HELP = 'Update a dynamic A/AAAA resource record.'

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--ipver'] = {
			'type': int,
			'metavar': 'VER',
			'choices': [4, 6],
			'help': 'use this IP version only to access the API'
		}
		ret['rrname'] = {
			'metavar': 'NAME',
			'help': 'the new/updated resource record name (w/o the zone)',
		}
		return ret

	def run (self):
		super().run()
		# The API strips the zone name from the record name for dynamic DNS
		# updates.  This makes the warning seemingly unnecessary because the
		# user likely gets the expected result, unless they really intend to
		# use host.example.org.example.org.  That said, this behaviour is not
		# consistent with all other commands, so let's print the warning
		# anyway to articulate a place where things can potentially go wrong.
		self.flag_zone_suffix (self.params.parsed['rrname'])
		msg = self.dnsclient.update_dynamic (
			self.zone_name,
			self.params.parsed['rrname']
		)
		print ('OK: %s' % msg)


class MBCLICommandDNSWithZoneAndTTL (MBCLICommandDNSWithZone):
	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--ttl'] = {
			'type': int,
			'help': 'use this resource record TTL'
		}
		return ret

	@abstractmethod
	def run (self):
		super().run()
		# TTL handling is special in MBCLICommandDNSAddStdin, which might
		# have enough input data not to require the user to specify TTL on
		# the command line or in the configuration file.  Hence declare the
		# option here, but check for it separately in each of the directly
		# derived classes.


class MBCLICommandDNSAddArgparse (MBCLICommandDNSWithZoneAndTTL):
	SUBCMD = re.compile ('^add-')

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['rrname'] = {
			'metavar': 'NAME',
			'help': 'the new resource record name (w/o the zone)',
		}
		ret['--replace'] = {
			'action': 'store_true',
			'help': 'replace all existing resource records of the same name and type'
		}
		ret.update (cls.RDATA_ARGS)
		return ret

	def run (self):
		super().run()
		self.flag_zone_suffix (self.params.parsed['rrname'])
		arg_keys = ['rrname'] + list(self.RDATA_ARGS.keys())
		workfunc = (
			self.dnsclient.replace_zone_record
			if self.params.parsed['replace']
			else self.dnsclient.add_zone_record
		)
		msg = workfunc (
			self.zone_name,
			MBDNSAPIRecordFactory.from_argparse_arg (
				self.params.cmdline_config ('default', 'ttl'),
				self.SUBCMD.sub ('', self.NAME),
				[self.params.parsed[k] for k in arg_keys]
			)
		)
		print ("OK: %s" % msg)


class MBCLICommandDNSAddA (MBCLICommandDNSAddArgparse):
	NAME = 'add-a'
	HELP = 'Add a new A resource record.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'ADDRESS',
			'help': 'IPv4 address (dotted-quad notation)',
		},
	)


class MBCLICommandDNSAddAAAA (MBCLICommandDNSAddArgparse):
	NAME = 'add-aaaa'
	HELP = 'Add a new AAAA resource record.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'ADDRESS',
			'help': 'IPv6 address',
		},
	)


class MBCLICommandDNSAddANAME (MBCLICommandDNSAddArgparse):
	NAME = 'add-aname'
	HELP = 'Add a new ANAME pseudo-RR.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'ANAME',
			'help': 'target hostname',
		},
	)


class MBCLICommandDNSAddCAA (MBCLICommandDNSAddArgparse):
	NAME = 'add-caa'
	HELP = 'Add a new CAA resource record.'
	RDATA_ARGS = OrderedDict (
		flags = {
			'metavar': 'FLAGS',
			'type': int,
			'choices': MBDNSAPIRecordCAA.valid_flags(),
		},
		tag = {
			'metavar': 'TAG',
			'choices': MBDNSAPIRecordCAA.valid_tags(),
		},
		value = {
			'metavar': 'VALUE',
			'help': 'a hexstring',
		},
	)


class MBCLICommandDNSAddCNAME (MBCLICommandDNSAddArgparse):
	NAME = 'add-cname'
	HELP = 'Add a new CNAME resource record.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'CNAME',
			'help': 'target hostname',
		},
	)


class MBCLICommandDNSAddDNAME (MBCLICommandDNSAddArgparse):
	NAME = 'add-dname'
	HELP = 'Add a new DNAME resource record.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'DNAME',
			'help': 'target domain',
		},
	)


class MBCLICommandDNSAddMX (MBCLICommandDNSAddArgparse):
	NAME = 'add-mx'
	HELP = 'Add a new MX resource record.'
	RDATA_ARGS = OrderedDict (
		preference = {
			'metavar': 'PREFERENCE',
			'type': int,
			'help': 'preference (lower is more preferred)',
		},
		exchange = {
			'metavar': 'EXCHANGE',
			'help': 'mail exchange hostname',
		},
	)


class MBCLICommandDNSAddNS (MBCLICommandDNSAddArgparse):
	NAME = 'add-ns'
	HELP = 'Add a new NS resource record.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'NSDNAME',
			'help': 'name server hostname',
		},
	)


class MBCLICommandDNSAddPTR (MBCLICommandDNSAddArgparse):
	NAME = 'add-ptr'
	HELP = 'Add a new PTR resource record.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'PTRDNAME',
			'help': 'source hostname',
		},
	)


class MBCLICommandDNSAddSRV (MBCLICommandDNSAddArgparse):
	NAME = 'add-srv'
	HELP = 'Add a new SRV resource record.'
	RDATA_ARGS = OrderedDict (
		priority = {
			'metavar': 'PRIORITY',
			'type': int,
			'help': 'priority (lower is more preferred)',
		},
		weight = {
			'metavar': 'WEIGHT',
			'type': int,
			'help': 'relative weight (higher is more preferred)',
		},
		port = {
			'metavar': 'PORT',
			'type': int,
			'help': 'service port number',
		},
		target = {
			'metavar': 'TARGET',
			'help': 'target hostname',
		},
	)


class MBCLICommandDNSAddSSHFP (MBCLICommandDNSAddArgparse):
	NAME = 'add-sshfp'
	HELP = 'Add a new SSHFP resource record.'
	RDATA_ARGS = OrderedDict (
		algorithm = {
			'metavar': 'ALGORITHM',
			'type': int,
			'choices': MBDNSAPIRecordSSHFP.valid_algos(),
			'help': 'public key algorithm',
		},
		fptype = {
			'metavar': 'FP-TYPE',
			'help': 'fingerprint type',
			'type': int,
			'choices': MBDNSAPIRecordSSHFP.valid_types(),
		},
		fingerprint = {
			'metavar': 'FINGERPRINT',
			'help': 'a hexstring',
		},
	)


class MBCLICommandDNSAddTLSA (MBCLICommandDNSAddArgparse):
	NAME = 'add-tlsa'
	HELP = 'Add a new TLSA resource record.'
	RDATA_ARGS = OrderedDict (
		usage = {
			'metavar': 'USAGE',
			'type': int,
			'choices': MBDNSAPIRecordTLSA.valid_usages(),
			'help': 'certificate usage',
		},
		selector = {
			'metavar': 'SELECTOR',
			'type': int,
			'choices': MBDNSAPIRecordTLSA.valid_selectors(),
		},
		mtype = {
			'metavar': 'MTYPE',
			'type': int,
			'choices': MBDNSAPIRecordTLSA.valid_mtypes(),
			'help': 'matching type',
		},
		cadata = {
			'metavar': 'CADATA',
			'help': 'certificate association data (a hexstring)',
		},
	)


class MBCLICommandDNSAddTXT (MBCLICommandDNSAddArgparse):
	NAME = 'add-txt'
	HELP = 'Add a new TXT resource record.'
	RDATA_ARGS = OrderedDict (
		rdata = {
			'metavar': 'TXT-DATA',
			'help': 'a text string',
		},
	)


class MBCLICommandDNSAddStdin (MBCLICommandDNSWithZoneAndTTL):
	NAME = 'add-stdin'
	HELP = 'Add a new resource record for each master file line provided on \
		standard input (one RR per line).  If --ttl is specified, it will \
		override each RR TTL, otherwise the input RR TTL will be used if \
		present, or the configured default TTL as last resort.  Comments are \
		supported only if they start the line.'

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['name'] = {
			'nargs': '?',
			'metavar': 'NAME',
			'help': 'use this as the new record(s) name instead of the input data',
		}
		return ret

	@classmethod
	def patterns (cls) -> list:
		def pattern (items: tuple) -> str:
			return re.compile ( '^%s$' % r'[ \t]+'.join (items), re.IGNORECASE)

		re_name = r'(?P<name>[a-z0-9\._-]+)'
		re_ttl = '(?P<ttl>[0-9]+)'
		re_class = '(?P<class>[a-z]{2})'
		re_type = '(?P<type>%s)' % '|'.join (MBDNSAPIRecordFactory.rrtypes())
		re_rdata = '(?P<rdata>.+)'
		return [
			pattern ([re_name, re_type, re_rdata]),
			# According to RFC 1035 Section 5.1, both <TTL> and <class> are optional
			# and may appear either way round.
			pattern ([re_name, re_class, re_type, re_rdata]),
			pattern ([re_name, re_ttl, re_type, re_rdata]),
			pattern ([re_name, re_class, re_ttl, re_type, re_rdata]),
			pattern ([re_name, re_ttl, re_class, re_type, re_rdata]),
		]

	def parse_master_file (self, name, lines: list) -> list:
		'''Convert master file lines to MBDNSAPIRecord instances.'''
		ret = []
		for line in lines:
			line = self.comment.sub ('', line)
			line = line.strip()
			if len (line) == 0:
				continue
			for pattern in self.patterns:
				m = pattern.match (line)
				if m is not None:
					rr = m.groupdict()
					break
			else:
				raise ValueError ("not a valid master file line: '%s'" % line)
			if 'class' in rr and rr['class'].upper() != 'IN':
				raise ValueError ("master file line RR class is '%s', not 'IN'" % rr['class'])
			rrttl = self.params.parsed['ttl']
			if rrttl is None:
				rrttl = rr['ttl'] if 'ttl' in rr else self.params.config ('default', 'ttl')
			ret.append (MBDNSAPIRecordFactory.from_master_file_line (
				rrttl,
				rr['type'],
				name if name is not None else rr['name'],
				rr['rdata']
			))
		return ret

	def run (self):
		super().run()
		self.comment = re.compile ('^;.*')
		self.patterns = self.patterns()
		parsed = self.parse_master_file (self.params.parsed['name'], sys.stdin)
		print ("OK: parsed %u resource record(s)" % len (parsed))
		for rr in parsed:
			self.dnsclient.add_zone_record (self.zone_name, rr)
		print ("OK: added %u resource record(s)" % len (parsed))


class MBCLICommandDomains (MBCLICommand):
	NAME = 'domains'
	HELP = 'Manage registered DNS domains.'

	@abstractmethod
	def run (self):
		super().run()
		self.domclient = MBDomainAPIClient (self.authclient)


class MBCLICommandDomainsList (MBCLICommandDomains):
	NAME = 'ls'
	HELP = 'List all registered DNS domains.'

	def run (self):
		super().run()
		self.print_list_default (
			self.domclient.get_domain_list(),
			self.params.config_none ('default', 'domain')
		)


class MBCLICommandDomainsWithDomain (MBCLICommandDomains):
	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--domain'] = {
			'help': 'use this DNS domain',
		}
		return ret

	@abstractmethod
	def run (self):
		super().run()
		self.domain_name = self.params.cmdline_config ('default', 'domain')


class MBCLICommandDomainsShow (MBCLICommandDomainsWithDomain):
	NAME = 'show'
	HELP = 'Print detailed information about a DNS domain.'
	ENABLED = {
		True: 'Enabled',
		False: 'Disabled',
	}
	# https://www.iana.org/assignments/dns-sec-alg-numbers/dns-sec-alg-numbers.xhtml#dns-sec-alg-numbers-1
	DSALGO = {
		0: 'DELETE',
		1: 'RSAMD5',
		2: 'DH',
		3: 'DSA',
		5: 'RSASHA1',
		6: 'DSA-NSEC3-SHA1',
		7: 'RSASHA1-NSEC3-SHA1',
		8: 'RSASHA256',
		10: 'RSASHA512',
		12: 'ECC-GOST',
		13: 'ECDSAP256SHA256',
		14: 'ECDSAP384SHA384',
		15: 'ED25519',
		16: 'ED448',
		252: 'INDIRECT',
		253: 'PRIVATEDNS',
		254: 'PRIVATEOID',
	}
	# https://www.iana.org/assignments/ds-rr-types/ds-rr-types.xhtml#ds-rr-types-1
	DSDIGEST = {
		0: 'reserved',
		1: 'SHA-1',
		2: 'SHA-256',
		3: 'GOST R 34.11-94',
		4: 'SHA-384',
	}

	@classmethod
	def ds_algo (cls, x: int) -> str:
		'''Decode a DS algorithm code point.'''
		if 17 <= x <= 122:
			return 'unassigned'
		return cls.DSALGO.get (x, 'reserved')

	@classmethod
	def ds_digest (cls, x: int) -> str:
		'''Decode a DS digest code point.'''
		return cls.DSDIGEST.get (x, 'unassigned')

	def run (self):
		super().run()
		domain = self.domclient.get_domain (self.domain_name)
		summary = [
			('Name', domain['domain']),
			('Status', domain['status']),
			('Registered until', domain['expiry']),
			('DNS service', self.ENABLED[domain['dns']]),
			('Managed DNSSEC', self.ENABLED[domain['dnssec']]),
			('Lock', self.ENABLED[domain['locked']]),
		]
		if 'nameservers' in domain:
			for ns in domain['nameservers']:
				nstmp = [ns['name']]
				for af in ['ipv4', 'ipv6']:
					if af in ns:
						nstmp.append (ns[af])
				summary.append (('Name server', ' '.join (nstmp)))
		if 'ds' in domain:
			for ds in domain['ds']:
				summary.extend ([
					('DS key tag', ds['key_tag']),
					('DS algorithm', '%s (%s)' %
						(ds['algorithm'], self.ds_algo (ds['algorithm']))),
					('DS digest type', '%s (%s)' %
						(ds['digest_type'], self.ds_digest (ds['digest_type']))),
					('DS digest', ds['digest']),
				])
		self.print_header_data (summary)


class MBCLICommandDomainsWithDomainOnOff (MBCLICommandDomainsWithDomain):
	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['state'] = {
			'choices': cls.STRTOBOOL.keys(),
			'metavar': 'STATE',
		}
		return ret

	@abstractmethod
	def run (self):
		super().run()
		self.state = self.STRTOBOOL[self.params.parsed['state'].lower()]


class MBCLICommandDomainsLock (MBCLICommandDomainsWithDomainOnOff):
	NAME = 'lock'
	HELP = 'Lock or unlock a domain.'

	def run (self):
		super().run()
		self.domclient.set_lock (self.domain_name, self.state)
		print ('OK')


class MBCLICommandDomainsDNS (MBCLICommandDomainsWithDomainOnOff):
	NAME = 'dns'
	HELP = 'Enable or disable DNS service for a domain.'

	def run (self):
		super().run()
		self.domclient.set_dns (self.domain_name, self.state)
		print ('OK')


class MBCLICommandDomainsDNSSEC (MBCLICommandDomainsWithDomainOnOff):
	NAME = 'dnssec'
	HELP = 'Enable managed DNSSEC service for a domain.'

	def run (self):
		super().run()
		if not self.state:
			raise MBCLIParamsError ('The API does not support disabling DNSSEC.')
		self.domclient.set_dnssec (self.domain_name, self.state)
		print ('OK')


class MBCLICommandRPi (MBCLICommand):
	NAME = 'rpi'
	HELP = 'Manage Raspberry Pi servers.'

	@abstractmethod
	def run (self):
		super().run()
		self.rpiclient = MBRPiAPIClient (self.authclient)


class MBCLICommandRPiWithModel (MBCLICommandRPi):
	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--rpi-model'] = {
			'type': int,
			'choices': [3, 4],
			'help': 'use this Raspberry Pi model'
		}
		return ret

	@abstractmethod
	def run (self):
		super().run()
		# Comes as a string if from configparser.
		self.model = int (self.params.cmdline_config ('default', 'rpi_model'))


class MBCLICommandRPiImages (MBCLICommandRPiWithModel):
	NAME = 'images'
	HELP = 'List all OS images for a Raspberry Pi model.'

	def run (self):
		super().run()
		images = self.rpiclient.get_images (self.model)
		self.print_header_data (self.sorted_tuples (images))


class MBCLICommandRPiLs (MBCLICommandRPi):
	NAME = 'ls'
	HELP = 'List all Raspberry Pi servers.'

	def run (self):
		super().run()
		servers = self.rpiclient.get_servers()
		self.print_list_default (
			["%s [%s]" % (k, servers[k]['model']) for k in sorted (servers.keys())],
			self.params.config_none ('default', 'rpi_id')
		)


class MBCLICommandRPiWithID (MBCLICommandRPi):
	RPI_ID = re.compile ('^[a-z0-9-]{3,}$', re.IGNORECASE)

	@classmethod
	def parse_rpi_id (cls, s: str) -> str:
		if cls.RPI_ID.match (s) is None:
			raise argparse.ArgumentTypeError (
				"'%s' is not a valid ID (must be >= 3 characters, alnum and dashes only)" % s
			)
		return s

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--rpi-id'] = {
			'help': 'use this Raspberry Pi server ID',
			'metavar': 'RPI-ID',
			'type': cls.parse_rpi_id,
		}
		return ret

	@abstractmethod
	def run (self):
		super().run()
		self.rpi_id = self.params.cmdline_config ('default', 'rpi_id')


class MBCLICommandRPiShow (MBCLICommandRPiWithID):
	NAME = 'show'
	HELP = 'Print detailed information about a Raspberry Pi server.'

	def run (self):
		super().run()
		server = self.rpiclient.get_server (self.rpi_id)
		self.print_header_data (self.sorted_tuples (server))


class MBCLICommandRPiGetKeys (MBCLICommandRPiWithID):
	NAME = 'get-keys'
	HELP = 'Print authorized public SSH keys for a Raspberry Pi server.'

	def run (self):
		super().run()
		auth_keys = self.rpiclient.get_authorized_keys (self.rpi_id)
		auth_keys = auth_keys.rstrip ("\n")
		if len (auth_keys):
			print (auth_keys)


class MBCLICommandRPiSetKeys (MBCLICommandRPiWithID):
	NAME = 'set-keys'
	HELP = 'Replace authorized public SSH keys for a Raspberry Pi server.'

	@classmethod
	def make_args (self) -> OrderedDict:
		ret = super().make_args()
		ret['file'] = {
			'type': argparse.FileType ('r'),
			'metavar': 'FILENAME',
			'help': 'replace authorized_keys on the server with this file',
		}
		return ret

	def run (self):
		super().run()
		self.rpiclient.set_authorized_keys (self.rpi_id,
			self.params.parsed['file'].read()
		)
		print ('OK')


class MBCLICommandRPiReboot (MBCLICommandRPiWithID):
	NAME = 'reboot'
	HELP = 'Reboot a Raspberry Pi server.'

	def run (self):
		super().run()
		msg = self.rpiclient.reboot (self.rpi_id)
		print ('OK: %s' % msg)


class MBCLICommandRPiPower (MBCLICommandRPiWithID):
	NAME = 'power'
	HELP = 'Manage a Raspberry Pi server power supply.'

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['state'] = {
			'choices': cls.STRTOBOOL.keys(),
			'metavar': 'STATE',
		}
		return ret

	def run (self):
		super().run()
		msg = self.rpiclient.set_power (self.rpi_id,
			self.STRTOBOOL[self.params.parsed['state'].lower()]
		)
		print ('OK: %s' % msg)


class MBCLICommandRPiAdd (MBCLICommandRPiWithID, MBCLICommandRPiWithModel):
	NAME = 'add'
	HELP = 'Provision a new Raspberry Pi server.'

	@staticmethod
	def parse_disk_size (s: str) -> int:
		try:
			ret = int (s)
		except Exception as e:
			raise argparse.ArgumentTypeError ("'%s' is not an integer" % s) from e
		if ret < 0:
			raise argparse.ArgumentTypeError ("%d is negative" % ret)
		if ret % 10:
			raise argparse.ArgumentTypeError ("%u is not a multiple of 10" % ret)
		return ret

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--disk-size'] = {
			'metavar': 'GB',
			'type': cls.parse_disk_size,
			'help': 'use this much disk space',
		}
		ret['--image'] = {
			'metavar': 'NAME',
			'help': 'use this operating system image',
		}
		return ret

	def run (self):
		super().run()
		kwargs = {'model': self.model}
		if 'disk_size' in self.params.parsed:
			kwargs['disk'] = self.params.parsed['disk_size']
		if 'image' in self.params.parsed:
			kwargs['os_image'] = self.params.parsed['image']
		self.rpiclient.provision (self.rpi_id, **kwargs)
		print ('OK')


class MBCLICommandRPiRm (MBCLICommandRPiWithID):
	NAME = 'rm'
	HELP = 'Unprovision a Raspberry Pi server.'

	def run (self):
		super().run()
		msg = self.rpiclient.unprovision (self.rpi_id)
		print ('OK: %s' % msg)


class MBCLICommandVS (MBCLICommand):
	NAME = 'vs'
	HELP = 'Manage virtual servers.'

	@abstractmethod
	def run (self):
		super().run()
		self.vsclient = MBVSAPIClient (self.authclient)


class MBCLICommandVSProducts (MBCLICommandVS):
	NAME = 'products'
	HELP = 'List all virtual server products.'

	def run (self):
		super().run()
		products = {
			i['code']: i['description']
			for i in self.vsclient.get_products().values()
		}
		self.print_header_data (self.sorted_tuples (products))


class MBCLICommandVSImages (MBCLICommandVS):
	NAME = 'images'
	HELP = 'List all OS images for virtual servers.'

	def run (self):
		super().run()
		images = {
			i['description']: i['name']
			for i in self.vsclient.get_images().values()
		}
		self.print_header_data (self.sorted_tuples (images))


class MBCLICommandVSLs (MBCLICommandVS):
	NAME = 'ls'
	HELP = 'List all virtual servers.'

	def run (self):
		super().run()
		servers = {
			'%s [%s]' % (i['identifier'], i['product']):
			'dormant' if i['dormant'] else 'active'
			for i in self.vsclient.get_servers().values()
		}
		self.print_header_data (self.sorted_tuples (servers))


class MBCLICommandVSWithID (MBCLICommandVS):
	# "It must consist of letters and digits, and be between 3 and 20 characters
	# long."
	VS_ID = re.compile ('^[a-z0-9]{3,20}$')

	@classmethod
	def parse_vs_id (cls, s: str) -> str:
		if cls.VS_ID.match (s) is None:
			raise argparse.ArgumentTypeError (
				"'%s' is not a valid ID (must be between 3 and 20"
				" alphanumeric characters)" % s
			)
		return s

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--vs-id'] = {
			'help': 'use this virtual server ID',
			'metavar': 'ID',
			'type': cls.parse_vs_id,
		}
		return ret

	@abstractmethod
	def run (self):
		super().run()
		self.vs_id = self.params.cmdline_config ('default', 'vs_id')


class MBCLICommandVSShow (MBCLICommandVSWithID):
	NAME = 'show'
	HELP = 'Show detailed information about a virtual server.'

	def run (self):
		super().run()
		vs = self.vsclient.get_server (self.vs_id)
		rows = {
			'Identifier': vs['identifier'],
			'Product code': vs['product'],
			'Cost': '£%.2f/%s' % (vs['price'] / 100, vs['period']),
			'Dormant': vs['dormant'],
			'Status': vs['status'],
			'IPv4 addresses': ', '.join (vs['ipv4']),
			'IPv6 addresses': ', '.join (vs['ipv6']),
			'Location': '%s / %s' % (vs['zone']['name'], vs['host_server']),
			'RAM': '%uMB' % vs['specs']['ram'],
			'CPU cores': vs['specs']['cores'],
			'CPU extra cores': str (vs['specs'].get ('extra_cores', 0)),
			'CPU mode': vs['cpu_mode'],
			'Disk': '%uMB %s' % (
				vs['specs']['disk_size'],
				vs['specs']['disk_type'].upper()
			),
		}
		self.print_header_data (rows.items())


class MBCLICommandVSConsole (MBCLICommandVSWithID):
	NAME = 'console'
	HELP = 'Print console output for a virtual server.'

	@classmethod
	def make_args (cls) -> OrderedDict:
		ret = super().make_args()
		ret['--lines'] = {
			'help': 'print no more than this many lines',
			'metavar': 'NUM',
			'type': int,
		}
		return ret

	def run (self):
		super().run()
		output = self.vsclient.get_console_output (self.vs_id,
			limit = self.params.parsed['lines'])
		output = output.rstrip ("\n")
		if len (output):
			print (output)


class MBCLI:
	DESCRIPTION = 'CLI for Mythic Beasts APIs -- a free software for \
		command-line interface management of some services provided by \
		Mythic Beasts. See https://gitlab.com/dovsienko/mbcli for the \
		most recent version.'
	SYNOPSIS = [
		(
			MBCLICommandDNS,
			[
				MBCLICommandDNSRRTypes,
				MBCLICommandDNSZones,
				MBCLICommandDNSLs,
				MBCLICommandDNSUpdDynamic,
				MBCLICommandDNSAddA,
				MBCLICommandDNSAddAAAA,
				MBCLICommandDNSAddANAME,
				MBCLICommandDNSAddCAA,
				MBCLICommandDNSAddCNAME,
				MBCLICommandDNSAddDNAME,
				MBCLICommandDNSAddMX,
				MBCLICommandDNSAddNS,
				MBCLICommandDNSAddPTR,
				MBCLICommandDNSAddSRV,
				MBCLICommandDNSAddSSHFP,
				MBCLICommandDNSAddTLSA,
				MBCLICommandDNSAddTXT,
				MBCLICommandDNSAddStdin,
				MBCLICommandDNSRm,
			]
		),
		(
			MBCLICommandDomains,
			[
				MBCLICommandDomainsList,
				MBCLICommandDomainsShow,
				MBCLICommandDomainsLock,
				MBCLICommandDomainsDNS,
				MBCLICommandDomainsDNSSEC,
			]
		),
		(
			MBCLICommandRPi,
			[
				MBCLICommandRPiImages,
				MBCLICommandRPiLs,
				MBCLICommandRPiShow,
				MBCLICommandRPiGetKeys,
				MBCLICommandRPiSetKeys,
				MBCLICommandRPiReboot,
				MBCLICommandRPiPower,
				MBCLICommandRPiAdd,
				MBCLICommandRPiRm,
			]
		),
		(
			MBCLICommandVS,
			[
				MBCLICommandVSProducts,
				MBCLICommandVSImages,
				MBCLICommandVSLs,
				MBCLICommandVSShow,
				MBCLICommandVSConsole,
			]
		),
	]

	def __init__ (self, configured: ConfigParser):
		self.configured = configured
		self.ap = argparse.ArgumentParser (description = self.DESCRIPTION)
		self.ap.add_argument (
			'--debug',
			action = 'store_true',
			help = 'print debugging information for failed API requests'
		)
		self.ap.add_argument (
			'--timeout',
			metavar = 'SECONDS',
			type = float,
			help = 'set a timeout for all API requests'
		)
		self.cmd_help = {}
		self.subcmd_help = {}
		cmdsubparser = self.ap.add_subparsers()
		for cmdclass, subcmdclasses in self.SYNOPSIS:
			cmdparser = cmdclass.make_parser (cmdsubparser)
			if len (subcmdclasses):
				subcmdsubparser = cmdparser.add_subparsers()
				for subcmdclass in subcmdclasses:
					subcmdparser = subcmdclass.make_parser (subcmdsubparser)
					self.subcmd_help[subcmdclass] = subcmdparser.format_help()
			self.cmd_help[cmdclass] = cmdparser.format_help()

	@staticmethod
	def print_error_line (s: str):
		print ("ERROR: %s" % s, file = sys.stderr)

	@staticmethod
	def usage (s: str):
		print ("%s" % s, file = sys.stderr)
		return os.EX_USAGE

	def run (self, argv: list) -> int:
		# The parser calls exit() if the arguments were invalid or if it is
		# a valid request with -h/--help.
		try:
			vargs = vars (self.ap.parse_args (argv))
		except SystemExit as e:
			return (e.code)
		if 'worker' not in vargs:  # no command
			return self.usage (self.ap.format_help())
		if vargs['worker'] in self.cmd_help:  # no subcommand
			return self.usage (self.cmd_help[vargs['worker']])
		cmd = vargs['worker'] (MBCLIParams (vargs, self.configured))
		try:
			cmd.run()
		except ValueError as e:
			return self.usage (
				"ERROR: %s\n\n%s" % (e, self.subcmd_help[vargs['worker']])
			)
		except MBAPIError as e:
			self.print_error_line (e)
			if not vargs['debug'] and e.is_documented:
				if e.details:
					for s in e.details:
						self.print_error_line ('API: ' + s)
				else:
					self.print_error_line ('(Use "--debug" for detailed diagnostics.)')
			else:
				for header, data in e.get_diagnostics():
					self.print_error_line ("DEBUG: %s: %s" % (header, data))
			return e.get_exit_status()
		return 0

if __name__ == '__main__':
	try:
		configured = ConfigParser()
		configured.read (os.path.join (os.path.dirname (__file__), 'mbcli.conf'))
		cli = MBCLI (configured)
		exit (cli.run (sys.argv[1:]))
	except MBCLIParamsError as e:
		MBCLI.print_error_line ('Parameter error: %s' % str (e))
		exit (os.EX_CONFIG)
	except RuntimeError as e:
		MBCLI.print_error_line (e)
		# Exit with the default status.
	except requests.exceptions.RequestException as e:
		MBCLI.print_error_line ('Request failed: %s' % str (e))
		exit (os.EX_PROTOCOL)
	except Exception as e:
		MBCLI.print_error_line (str (e))
		exit (1)
	exit (os.EX_SOFTWARE)

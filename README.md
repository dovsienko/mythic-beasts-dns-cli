## NAME
`mbcli.py` — CLI for Mythic Beasts APIs

## DISCLAIMER
This software is neither developed nor maintained by the Mythic Beasts company.

## SYNOPSIS
```
mbcli.py domains ls
mbcli.py domains show [--domain <DOMAIN>]
mbcli.py domains lock [--domain <DOMAIN>] <STATE>
mbcli.py domains dns [--domain <DOMAIN>] <STATE>
mbcli.py domains dnssec [--domain <DOMAIN>] <STATE>
mbcli.py dns rrtypes
mbcli.py dns zones
mbcli.py dns ls [--zone <ZONE>] [--name <RRNAME>] [--type <RRTYPE>]
mbcli.py dns upd-dynamic [--zone <ZONE>] [--ipver <VER>] <NAME>
mbcli.py dns add-a [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <ADDRESS>
mbcli.py dns add-aaaa [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <ADDRESS>
mbcli.py dns add-aname [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <ANAME>
mbcli.py dns add-caa [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <FLAGS> <TAG> <VALUE>
mbcli.py dns add-cname [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <CNAME>
mbcli.py dns add-dname [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <DNAME>
mbcli.py dns add-mx [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <PREFERENCE> <EXCHANGE>
mbcli.py dns add-ns [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <NSDNAME>
mbcli.py dns add-ptr [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <PTRDNAME>
mbcli.py dns add-srv [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <PRIORITY> <WEIGHT> <PORT> <TARGET>
mbcli.py dns add-sshfp [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <ALGORITHM> <FP-TYPE> <FINGERPRINT>
mbcli.py dns add-tlsa [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <USAGE> <SELECTOR> <MTYPE> <CADATA>
mbcli.py dns add-txt [--replace] [--zone <ZONE>] [--ttl <TTL>] <NAME> <TXT-DATA>
mbcli.py dns add-stdin [--zone <ZONE>] [--ttl <TTL>] [<NAME> ]
mbcli.py dns rm [--zone <ZONE>] <NAME> <TYPE>
mbcli.py rpi images [--rpi-model <MODEL>]
mbcli.py rpi ls
mbcli.py rpi show [--rpi-id <RPI-ID>]
mbcli.py rpi get-keys [--rpi-id <RPI-ID>]
mbcli.py rpi set-keys [--rpi-id <RPI-ID>] <FILENAME>
mbcli.py rpi reboot [--rpi-id <RPI-ID>]
mbcli.py rpi power [--rpi-id <RPI-ID>] <STATE>
mbcli.py rpi add [--rpi-model <MODEL>] [--rpi-id <RPI-ID>] [--disk-size <GB>] [--image <NAME>]
mbcli.py rpi rm [--rpi-id <RPI-ID>]
mbcli.py vs products
mbcli.py vs images
mbcli.py vs ls
mbcli.py vs show [--vs-id <ID>]
mbcli.py vs console [--vs-id <ID>] [--lines <NUM>]
mbcli.py [--debug] <COMMAND> [<ARGS>]
mbcli.py --help
```

## DESCRIPTION
This small Python program implements a simple command-line interface to some
services provided by [Mythic Beasts](https://www.mythic-beasts.com/):
* It can display and update records in hosted DNS zones using the
  [Primary DNS API v2](https://www.mythic-beasts.com/support/api/dnsv2).
* It can display information about registered DNS domains and manage some of
  their aspects using the
  [Domain API](https://www.mythic-beasts.com/support/api/domains).
* It can display information about Raspberry Pi servers and manage some of
  their aspects using the
  [Paspberry Pi Provisioning API](https://www.mythic-beasts.com/support/api/raspberry-pi).
* It can display basic information about virtual servers using the
  [Virtual Server Provisioning API](https://www.mythic-beasts.com/support/api/vps).

To work with these APIs it authenticates at the
[Auth service](https://www.mythic-beasts.com/support/api/auth) using a
user-provided API key.

## REQUIREMENTS
This program has been tested to work with Python 3.8–3.12 on GNU/Linux, it
should work with other versions of Python 3 and other operating systems.  It
depends on the Requests library, which might have to be installed separately,
for example:
* as `python3-requests` in many Linux distributions, including Debian and its
  derivatives, Fedora and openSUSE;
* as `py39-requests` in FreeBSD ports;
* as `py310-requests` in NetBSD pkgsrc;
* as `py3-requests` in OpenBSD ports and Alpine Linux;
* as `requests_python310` in Haiku ports;
* as `library/python/requests-39` in Solaris 11 packages;
* as `library/python/requests-39` in OpenIndiana packages.

## INITIAL CONFIGURATION
1. Log into Mythic Beasts web-interface and go to "My Account", "API keys",
   "Create API key" (possibly
   [here](https://www.mythic-beasts.com/customer/api-users/create)).
2. Fill in the new key name and access details. Grant only as many permits as
   necessary, in particular:
   * Under "Primary DNS API v2" click "Add permit" and carefully specify which
     zones, records and record types need to be writable using the new key.
     As the DNS zone(s) of interest must have at least one permit to be
     available for reading, you can grant read-only access to a zone by
     specifying a record name that has no function.
   * Under "Domains API" click "Add permit" and carefully specify which domains
     need to be readable using the new key. Do not tick any of the checkboxes
     with specific scopes.
   * Under "Raspberry Pi Provisioning" tick the checkbox if you need to manage
     this service from the CLI.
3. Click "Create API key" and take note of the key ID and secret.
4. `cp mbcli.conf.example mbcli.conf`
5. Configure the parameters: `$EDITOR mbcli.conf`

## WORKING WITH REGISTERED DOMAINS
Test the API key Domain API permissions:
```
mbcli.py domains ls
```
Print summary information about the default domain:
```
mbcli.py domains show
```
Lock or unlock a domain:
```
mbcli.py domains lock on
mbcli.py domains lock --domain example.org off
```
Enable DNS service for a domain:
```
mbcli.py domains dns --domain example.org on
```
Enable managed DNSSEC service for a domain:
```
mbcli.py domains dnssec --domain example.org on
```

## WORKING WITH DNS ZONES
Test the API key credentials:
```
$ mbcli.py dns rrtypes
A:     host, ttl, type, data
AAAA:  host, ttl, type, data
ANAME: host, ttl, type, data
CAA:   host, ttl, type, caa_flags, caa_tag, data
CNAME: host, ttl, type, data
DNAME: host, ttl, type, data
MX:    host, ttl, type, mx_priority, data
NS:    host, ttl, type, data
PTR:   host, ttl, type, data
SRV:   host, ttl, type, srv_priority, srv_weight, srv_port, data
SSHFP: host, ttl, type, sshfp_algorithm, sshfp_type, data
TLSA:  host, ttl, type, tlsa_usage, tlsa_selector, tlsa_matching, data
TXT:   host, ttl, type, data
```
Test the API key DNS API v2 permissions:
```
mbcli.py dns zones
```
List records in the default zone:
```
mbcli.py dns ls
```

### How to manage static resource records
Add IPv4 and IPv6 addresses for a host:
```
mbcli.py dns add-a --zone example.org somehost 192.0.2.123
mbcli.py dns add-aaaa --zone example.org somehost 2001:db8::abcd
```
Add PTR records:
```
mbcli.py dns add-ptr --zone 2.0.192.in-addr.arpa 123 somehost.example.org.
mbcli.py dns add-ptr --zone 0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6.arpa d.c.b.a.0.0.0.0.0.0.0.0.0.0.0.0 somehost.example.org.
```
Replace IPv4 address for a host:
```
mbcli.py dns add-a --replace --zone example.org somehost 192.0.2.45
```
Add SPF data:
```
mbcli.py dns add-txt --zone example.org somehost 'v=spf1 a -all'
```
Add MX records for a subdomain:
```
mbcli.py dns add-mx --zone example.org subdomain 10 relay1.subdomain
mbcli.py dns add-mx --zone example.org subdomain 20 relay2.subdomain
```

### How to add/update dynamic resource records
Use the client host's default (or the only) address family (this adds/updates
either the A RR only when connected via IPv4 or the AAAA RR only when connected
via IPv6):
```
mbcli.py dns upd-dynamic somename
```
Use IPv4 and/or IPv6 on a dual-stack client host:
```
mbcli.py dns upd-dynamic --ipv 4 somename
mbcli.py dns upd-dynamic --ipv 6 somename
```

### How to add zone records semi-automatically
Register a server's SSH public key fingerprints in DNS using an explicit TTL
value:
```
ssh-keyscan -D server.example.org |
mbcli.py dns add-stdin --zone example.org --ttl 7200 server
```
Copy all records for `host1.example.org` to `host2.example.org`, preserving the
TTL values:
```
mbcli.py dns ls --zone example.org --name host1 |
mbcli.py dns add-stdin --zone example.org host2
```
Copy all records for `host.domain1.org` to `host.domain2.org`, overriding the
TTL values (the API key can access both zones):
```
mbcli.py dns ls --zone domain1.org --name host |
mbcli.py dns add-stdin --zone domain2.org --ttl 900
```
Copy all records for `host.domain1.org` to `host.domain2.org`, preserving the
TTL values (the API key can access the destination zone only):
```
dig @ns.domain1.org host.domain1.org any |
mbcli.py dns add-stdin --zone domain2.org host
```

## WORKING WITH RASPBERRY PI SERVERS
List OS images available for specific models:
```
mbcli.py rpi images --rpi-model 3
mbcli.py rpi images --rpi-model 4
```
Provision a new Raspberry Pi server:
```
mbcli.py rpi add --rpi-id pi4server --rpi-model 4 --disk-size 20
```
List all provisioned servers:
```
mbcli.py rpi ls
```
Display information about a particular server:
```
mbcli.py rpi show --rpi-id pi4server
```
Print `authorized_keys` file of a particular server:
```
mbcli.py rpi get-keys --rpi-id pi4server
```
Replace `authorized_keys` file of a particular server:
```
mbcli.py rpi set-keys --rpi-id pi4server ~/.ssh/authorized_keys
```
Reboot a server:
```
mbcli.py rpi reboot --rpi-id pi4server
```
Power a server off:
```
mbcli.py rpi power --rpi-id pi4server off
```
Unprovision a Raspberry Pi server:
```
mbcli.py rpi rm --rpi-id pi4server
```

## EXIT STATUS
This program exits with the following status:
* 0 (`os.EX_OK`) if it succeeds
* 78 (`os.EX_CONFIG`) if there was a problem with the configuration file
* 64 (`os.EX_USAGE`) if the command-line arguments were invalid
* 65 (`os.EX_DATAERR`) if there was an error in the input data
* 67 (`os.EX_NOUSER`) if the API refused the key
* 77 (`os.EX_NOPERM`) if the API refused the request
* 73 (`os.EX_CANTCREAT`) if a new server cannot be provisioned due to a name
  clash or lack of hardware resources
* 76 (`os.EX_PROTOCOL`) if the API returned an invalid response (in this case
  the program will also print the request diagnostics)
* 70 (`os.EX_SOFTWARE`) if there was an internal error
* 1 on any other error

## SEE ALSO
* [dns-sync-zone](https://github.com/pwaring/dns-sync-zone) (Python)
* [hostedpi](https://github.com/piwheels/hostedpi) (Python)
* [mbdns](https://github.com/rys/mbdns) (Go)
* [MBDnsUpdater](https://github.com/langdt/MBDnsUpdater) (node.js)
* [mythic-dns](https://github.com/fooflington/mythic-dns) (Perl)
* [mythic-dns-api](https://github.com/nickmurison/mythic-dns-api) (Python)
* a few more in [this list](https://www.mythic-beasts.com/support/api/dnsv2/tools)

## AUTHORS
[CLI for Mythic Beasts APIs](https://gitlab.com/dovsienko/mbcli) was written
from scratch by Denis Ovsienko. See also [this file](COPYING) for the copyright
and licensing information.
